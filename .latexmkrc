$lualatex = 'lualatex -shell-escape %O %S';
$biber = 'biber %O %B';
$max_repeat = 10;
$pdf_previewer = 'xdg-open %D';