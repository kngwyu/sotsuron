# sotsuron

東京大学教養学部2019年度の卒業論文です

## コンパイル

```bash
latexmk -lualatex main.tex
```

## ライセンス

CC-BY 4.0
