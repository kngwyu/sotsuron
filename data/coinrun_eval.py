from matplotlib import pyplot as plt
import numpy as np


def boxp(ax, dat):
    return ax.boxplot(dat, sym='', positions=[1, 2, 3, 4], showmeans=True)


def box_config(box, mean, median, color, style):
    box.set_color(color)
    box.set_linestyle(style)
    mean.set_marker("x")
    mean.set_markeredgecolor(color)
    median.set_color(color)
    median.set_linewidth(2.0)

COLOR = True

escores = []
names = ['ppo', 'vppo', 'bppo', 'gppo', 'random']
for s in names[:-1]:
    fmt = 'coinrun/{}_color.npy' if COLOR else 'coinrun/{}.npy'
    escores.append(np.load(fmt.format(s)))
escores.append(np.load('coinrun/random.npy'))
# ax = plt.subplot(1, 1, 1)
# bp = boxp(ax, escores)
# plt.show()

