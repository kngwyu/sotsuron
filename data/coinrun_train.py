from matplotlib import pyplot as plt
import numpy as np

COLOR = False
XTICKS = np.array([0.1, 0.2, 0.4, 0.6, 0.8, 1.0]) * int(2e7)
XTICK_NAMES = ['{}M'.format(int(t / 1e6)) for t in XTICKS]
YTICKS = np.array([0.2, 0.4, 0.6, 0.8]) * 100
YTICK_NAMES = [f'{t}%' for t in YTICKS]

escores = []
names = ['ppo', 'vae', 'bvae', 'gvae']
prefix = 'color' if COLOR else 'mono'

logs = []
colors = ['blue', 'green', 'orange', 'pink']
for name in names:
    log = open_log(name + '-' + prefix)
    r = np.array(log['train']['reward-mean'])
    r2 = np.convolve(r, np.ones(10) / 10, mode='valid') * 10
    r = np.concatenate([r[:-len(r2)], r2])
    x = np.array(log['train']['update-steps']) * 16 * 100
    logs.append((x, r))

styles = ['-', '--', '-.', ':']
handles = []
for style, color, (x, y) in zip(styles, colors, logs):
    p, = plt.plot(x, y)
    p.set_color(color)
    p.set_linestyle(style)
    handles.append(p)

plt.xticks(XTICKS, XTICK_NAMES)
plt.yticks(YTICKS, YTICK_NAMES)
plt.xlabel('Frames used for training')
plt.ylabel('Solved stages')
plt.ylim([0, 90])
plt.legend(handles,
           ['PPO', 'βVAE-PPO(β=1)', 'βVAE-PPO(β=4)', 'γVAE-PPO'],
           bbox_to_anchor=(1, 0), loc='lower right')
plt.show()
