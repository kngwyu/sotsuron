require 'pathname'
base = 'kanagawa@libra-g0:/qnap/kanagawa/rogue-data/'
dir = Pathname('seed0-9/bvae')
fname = 'a.npy'
resultname = 'score.npy'
if not dir.exist?
  dir.mkdir()
end
(0..9).each{|i|
  p = Pathname(dir).join('seed' + i.to_s + '.npy')
  result = dir.join('seed' + i.to_s + resultname)
  cmd = 'scp -r ' + base + p.to_s + ' ' + result.to_s
  p cmd
  if not system(cmd)
    exit(-1)
  end
}
