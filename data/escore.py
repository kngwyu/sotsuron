from matplotlib import pyplot as plt

def boxp(ax, dat):
    return ax.boxplot(dat, sym='', positions=[1, 2, 3], showmeans=True)

def box_config(box, mean, median, color, style):
    box.set_color(color)
    box.set_linestyle(style)
    mean.set_marker("x")
    mean.set_markeredgecolor(color)
    median.set_color(color)
    median.set_linewidth(2.0)

escore = {}
for s, idx in zip(['ppo', 'vae_beta4', 'gammavae'], ['p', 'b', 'g']):
    escore[idx] = np.array([np.load('{}/seed{}result.npy'.format(s, i)) for i in range(10)])
p = escore['p'].mean(axis=2)
b = escore['b'].mean(axis=2)
g = escore['g'].mean(axis=2)
colors = ['blue', 'green', 'orange']
styles = ['-', '--', '-.']
for i in range(10):
    ax = plt.subplot(1, 12, i + 1)
    bp = boxp(ax, [p[i], b[i], g[i]])
    ax.set_xticklabels([])
    ax.set_ylim([0, 85])
    if i > 0:
        ax.set_yticklabels([])
    ax.set_xlabel('{}'.format(i))
    for box, mean, median, color, style in \
            zip(bp['boxes'], bp['means'], bp['medians'], colors, styles):
        box_config(box, mean, median, color, style)
    if i == 0:
        ax.set_ylabel('average scores')

ax = plt.subplot(1, 12, 11)
bp = boxp(ax, [np.load('seed0-9/{}_escore.npy'.format(s)).mean(axis=-1) for s in ['ppo', 'bvae', 'gvae']])
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_xlabel('0-9')
ax.set_ylim([0, 85])
for box, mean, median, color, style in \
    zip(bp['boxes'], bp['means'], bp['medians'], colors, styles):
    box_config(box, mean, median, color, style)
handles = [ax.plot([1, 1], color=colors[h], linestyle=styles[h])[0] for h in range(3)]

r = np.load('random/seed1000-2000.npy').mean(axis=1)
ax = plt.subplot(1, 16, 16)
bp = ax.boxplot(r, sym='', showmeans=True)
ax.set_xticklabels([])
ax.set_ylim([0, 85])
ax.set_yticklabels([])
ax.set_xlabel('random')
for box, mean, median in zip(bp['boxes'], bp['means'], bp['medians']):
    box_config(box, mean, median, 'pink', ':')
handles += [ax.plot([1, 1], color='pink', linestyle=':')[0]]
ax.legend(handles, ['PPO', 'βVAE-PPO', 'γVAE-PPO', 'random'])
for h in handles:
    h.set_visible(False)
plt.show()
