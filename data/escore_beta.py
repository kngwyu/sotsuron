from matplotlib import pyplot as plt
escore = {}
for s, idx in zip(['vae_beta1', 'vae_beta4', 'vae_beta8'], ['p', 'b', 'g']):
    escore[idx] = np.array([np.load('{}/seed{}result.npy'.format(s, i)) for i in range(5)])
p = escore['p'].mean(axis=2)
b = escore['b'].mean(axis=2)
g = escore['g'].mean(axis=2)
for i in range(5):
    ax = plt.subplot(1, 5, i + 1)
    bp = ax.boxplot(
        [p[i], b[i], g[i]],
        sym='',
        positions=[1, 2, 3],
        showmeans=True,
    )
    ax.set_xticklabels([])
    ax.set_ylim([0, 85])
    if i > 0:
        ax.set_yticklabels([])
    ax.set_xlabel('seed{}'.format(i))
    colors = ['purple', 'green', 'lightblue']
    styles = ['-', '--', '-.']
    for box, mean, median, color, style in \
            zip(bp['boxes'], bp['means'], bp['medians'], colors, styles):
        box.set_color(color)
        box.set_linestyle(style)
        mean.set_marker("x")
        mean.set_markeredgecolor(color)
        median.set_color(color)
        median.set_linewidth(2.0)
    if i == 0:
        ax.set_ylabel('average scores')
    handles = [None] * 3
    for h in range(3):
        handles[h], = ax.plot([1, 1], color=colors[h], linestyle=styles[h])
    if i == 4:
        ax.legend(handles, ['β=1.0', 'β=4.0', 'β=8.0'])
    for h in handles:
        h.set_visible(False)
plt.show()
