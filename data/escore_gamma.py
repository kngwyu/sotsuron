from matplotlib import pyplot as plt
escore = {}
for s, idx in zip(['gammavae', 'gamma400'], ['g2', 'g4']):
    escore[idx] = np.array([np.load('{}/seed{}result.npy'.format(s, i)) for i in range(5)])
g2 = escore['g2'].mean(axis=2)
g4 = escore['g4'].mean(axis=2)
for i in range(5):
    ax = plt.subplot(1, 5, i + 1)
    bp = ax.boxplot(
        [g2[i], g4[i]],
        sym='',
        positions=[1, 2],
        showmeans=True,
    )
    ax.set_xticklabels([])
    ax.set_ylim([0, 85])
    if i > 0:
        ax.set_yticklabels([])
    ax.set_xlabel('seed{}'.format(i))
    colors = ['purple', 'green', 'lightblue']
    styles = ['-', '--', '-.']
    for box, mean, median, color, style in \
            zip(bp['boxes'], bp['means'], bp['medians'], colors, styles):
        box.set_color(color)
        box.set_linestyle(style)
        mean.set_marker("x")
        mean.set_markeredgecolor(color)
        median.set_color(color)
        median.set_linewidth(2.0)
    if i == 0:
        ax.set_ylabel('normalized scores')
    handles = [ax.plot([1, 1], color=colors[h], linestyle=styles[h])[0] for h in range(2)]
    if i == 4:
        ax.legend(handles, ['γ=200.0', 'γ=400.0'])
    for h in handles:
        h.set_visible(False)
plt.show()
