# coding: utf-8
from matplotlib import pyplot as plt
tscore = {}
escore = {}
for s, idx in zip(['ppo', 'vae_beta4', 'gammavae'], ['p', 'b', 'g']):
    tscore[idx] = np.array([np.load('{}/seed{}score.npy'.format(s, i)) for i in range(10)])
    escore[idx] = np.array([np.load('{}/seed{}result.npy'.format(s, i)) for i in range(10)])
tmeans_p = tscore['p'].mean(axis=1)
tmeans_b = tscore['b'].mean(axis=1)
tmeans_g = tscore['g'].mean(axis=1)
tscore_bar = []
fig = plt.figure()
ax = fig.add_subplot(111)
bp = ax.boxplot([tmeans_p, tmeans_b, tmeans_g])
ax.set_xticklabels(['ppo', 'β-VAE', 'γ-VAE'])
plt.grid()
