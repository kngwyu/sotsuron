from matplotlib import pyplot as plt
tscore = {}
for s, idx in zip(['ppo', 'bvae', 'gvae'], ['p', 'b', 'g']):
    tscore[idx] = np.array([np.load('seed0-9/{}/seed{}score.npy'.format(s, i)) for i in range(10)])
r = np.array([np.load('random/seed{}.npy'.format(i)) for i in range(10)]).mean(axis=1)
p = tscore['p'].mean(axis=1)
b = tscore['b'].mean(axis=1)
g = tscore['g'].mean(axis=1)
for i in range(10):
    ax = plt.subplot(1, 10, i + 1)
    ph, = ax.bar([0], p[i], edgecolor='blue', linestyle='-', fill=False)
    bh, = ax.bar([1], b[i], edgecolor='green', linestyle='--', fill=False)
    gh, = ax.bar([2], g[i], edgecolor='orange', linestyle='-.', fill=False)
    rh, = ax.bar([3], r[i], edgecolor='pink', linestyle=':', fill=False)
    ax.set_ylim([0, 1400])
    ax.set_xticklabels([])
    if i > 0:
        ax.set_yticklabels([])
    ax.set_xlabel('seed{}'.format(i))
    if i == 0:
        ax.set_ylabel('average score')
    if i == 9:
        ax.legend([ph, bh, gh, rh], ['PPO', 'βVAE-PPO', 'γVAE-PPO', 'random'])
plt.show()
