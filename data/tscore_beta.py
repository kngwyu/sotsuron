from matplotlib import pyplot as plt
tscore = {}
for s, idx in zip(['vae_beta1', 'vae_beta4', 'vae_beta8'], ['p', 'b', 'g']):
    tscore[idx] = np.array([np.load('{}/seed{}score.npy'.format(s, i)) for i in range(5)])
p = tscore['p'].mean(axis=1)
b = tscore['b'].mean(axis=1)
g = tscore['g'].mean(axis=1)
for i in range(5):
    ax = plt.subplot(1, 5, i + 1)
    ph, = ax.bar([0], p[i], edgecolor='purple', linestyle='-', fill=False)
    bh, = ax.bar([1], b[i], edgecolor='green', linestyle='--', fill=False)
    gh, = ax.bar([2], g[i], edgecolor='lightblue', linestyle='-.', fill=False)
    ax.set_ylim([0, 1400])
    ax.set_xticklabels([])
    if i > 0:
        ax.set_yticklabels([])
    ax.set_xlabel('seed{}'.format(i))
    if i == 0:
        ax.set_ylabel('average score')
    if i == 4:
        ax.legend([ph, bh, gh], ['β=1.0', 'β=4.0', 'β=8.0'])
plt.show()
