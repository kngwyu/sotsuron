from matplotlib import pyplot as plt
tscore = {}
for s, idx in zip(['gammavae', 'gamma400'], ['p', 'b']):
    tscore[idx] = np.array([np.load('{}/seed{}score.npy'.format(s, i)) for i in range(5)])
p = tscore['p'].mean(axis=1)
b = tscore['b'].mean(axis=1)
for i in range(5):
    ax = plt.subplot(1, 5, i + 1)
    ph, = ax.bar([0], p[i], edgecolor='purple', linestyle='-', fill=False)
    bh, = ax.bar([1], b[i], edgecolor='green', linestyle='--', fill=False)
    ax.set_ylim([0, 1400])
    ax.set_xticklabels([])
    if i > 0:
        ax.set_yticklabels([])
    ax.set_xlabel('seed{}'.format(i))
    if i == 0:
        ax.set_ylabel('normalized score')
    if i == 4:
        ax.legend([ph, bh], ['γ=200.0', 'γ=400.0'])
plt.show()
