\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {第1章}はじめに}{4}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {第2章}背景}{6}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}深層学習}{6}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}マルコフ決定過程}{7}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}部分観測マルコフ決定過程(POMDP)}{7}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}エピソード設定での強化学習}{7}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}価値ベースの手法}{8}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}方策ベースの手法}{9}{section.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.7}ドメイン適応}{12}{section.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {第3章}提案する学習手法}{16}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}提案手法の設計}{16}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}先行研究との関連性}{18}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {第4章}実験環境の作成}{20}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}既存の強化学習用実験環境}{20}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}既存の実験環境におけるドメイン適応}{21}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}提案する実験環境}{21}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {第5章}実験と評価}{23}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}実験環境}{23}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}比較する手法}{23}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}PPOと提案手法の比較}{24}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}\beta の増減によるスコアの変化}{26}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.5}ソース環境の多様性}{27}{section.5.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.6}Coinrun}{28}{section.5.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {第6章}まとめ}{31}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {付録A}}{38}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}実験環境の詳細}{38}{section.A.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.2}手法の詳細}{38}{section.A.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.3}Coinrunにおける実験設定}{41}{section.A.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.4}シードによる難しさの違い}{42}{section.A.4}
