const webpack = require('webpack');
const path = require('path');

module.exports = {
    mode: 'development',
    devServer: {
        contentBase: path.join(__dirname, '../'),
        open: "firefox",
        openPage: 'slide/index.html',
        port: 8000
    }
};
